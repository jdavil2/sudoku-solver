import numpy as np
import datetime as dt
from tkinter.filedialog import askopenfile


def possible(sudoku, x, y, n):
    for i in range(9):
        if sudoku[x][i] == n:
            return False
    for i in range(9):
        if sudoku[i][y] == n:
            return False
    # code to check the 3 by 3 that I was struggling with, didn't think of nor did I know of the floor division operator
    x0 = (x // 3) * 3
    y0 = (y // 3) * 3
    for i in range(3):
        for j in range(3):
            if sudoku[x0 + i][y0 + j] == n:
                return False
    return True


def solve(sudoku):
    for y in range(9):
        for x in range(9):
            if sudoku[y][x] == 0:
                # backtracking algorithm
                for n in range(1, 10):
                    if possible(sudoku, y, x, n):
                        sudoku[y][x] = n
                        solve(sudoku)
                        # if the sudoku has been solved return, which should preserve the solved sudoku
                        if not has_empty_spaces(sudoku):
                            return
                        sudoku[y][x] = 0
                return


def has_empty_spaces(sudoku):
    for i in range(9):
        for j in range(9):
            if sudoku[i][j] == 0:
                return True
    return False


def verify_row(row):
    check = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(9):
        for j in range(9):
            if i + 1 == row[j]:
                check[i] += 1
    for i in range(len(check)):
        if check[i] > 1:
            return False
    return True


def verify_3_by_3(sudoku):
    # current implementation skips a lot
    three_by_three = []
    for i in range(1, 9, 3):
        for j in range(1, 9, 3):
            y = (j // 3) * 3
            x = (i // 3) * 3
            for p in range(3):
                for z in range(3):
                    three_by_three.append((sudoku[x + p][y + z]))
    three_by_three = np.array(three_by_three)
    three_by_three = three_by_three.reshape(9, 9)
    for i in range(9):
        if not verify_row(three_by_three[i]):
            return False
    return True


def validate_sudoku(sudoku):
    sudoku_transposed = sudoku.transpose()
    for i in range(9):
        if not verify_row(sudoku[i]):
            return False
        if not verify_row(sudoku_transposed[i]):
            return False
    # at this point, if the other values are valid, it all depends if the 3 by 3's are valid, just return if they're valid
    return verify_3_by_3(sudoku)


def output_sudoku(sudoku, first_input, name):
    f = open("sudokuAnswer.txt", "a")
    if first_input:
        date = dt.datetime.now().strftime("%B %d,%Y")
        time = dt.datetime.now().strftime("%H:%M")
        f.write("The unsolved Sudoku\n" + "Sudoku filename: " + name + "\n" + date + "\n" + time + "\n")
    else:
        f.write("\nThe solved sudoku is: \n")
    for i in range(9):
        if i % 3 == 0:
            f.write(" -------------------\n")
        for j in range(9):
            if j % 3 == 0:
                f.write("|")
            if sudoku[i][j] == 0:
                f.write("  ")
            else:
                if j == 8:
                    f.write(str(sudoku[i][j]))
                else:
                    f.write(str(sudoku[i][j]) + " ")
        f.write("|")
        f.write("\n")
    f.write(" --------------------\n")
    f.write("\n\n\n\n")


def output_invalid(name):
    date = dt.datetime.now().strftime("%B %d,%Y")
    time = dt.datetime.now().strftime("%H:%M")
    f = open("sudokuAnswer.txt", "a")
    f.write(date + "\n" + time + "\n" + "Sudoku file: " + name + "Input sudoku is not valid\n\n\n\n")


def main():
    filename = askopenfile()
    name = filename.name
    name = name.split("/")
    read_data = filename.read()
    filename.close()
    sudoku_input = []
    for i in read_data:
        if not i == "\n" and not i == " ":
            if int(i) < 0 or int(i) > 9:
                output_invalid(name[-1])
                return
            sudoku_input.append(int(i))
    sudoku_input = np.array(sudoku_input)
    if not len(sudoku_input) == 81:
        output_invalid(name[-1])
        return
    sudoku_input = sudoku_input.reshape(9, 9)
    output_sudoku(sudoku_input, True, name[-1])
    if validate_sudoku(sudoku_input):
        solve(sudoku_input)
        output_sudoku(sudoku_input, False, name[-1])
        return
    output_invalid(name[-1])


if __name__ == "__main__":
    main()
